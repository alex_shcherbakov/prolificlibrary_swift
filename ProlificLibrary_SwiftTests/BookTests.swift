//
//  BookTests.swift
//  ProlificLibrary_Swift
//
//  Created by Alexander Shcherbakov on 1/14/16.
//  Copyright © 2016 Oleksandr Shcherbakov. All rights reserved.
//

import XCTest
@testable import ProlificLibrary_Swift

class BookTests: XCTestCase {

    var book: Book!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        super.tearDown()
    }

    func testBookCreatedByUser() {
        // When
        book = Book(title: "Title", categories: "Categories", publisher: "Publisher", author: "Author")
        
        // Then
        XCTAssertEqual(book.title, "Title")
        XCTAssertNil(book.lastCheckedOut, "Created book should not be checked out")
    }
    
    func testBookFromAPI() {
        // When
        book = Book(json: BookHelper.fakeJSONcheckedOut())
        
        // Then
        XCTAssertEqual(book.title, "Running Lean")
        XCTAssertNotNil(book.lastCheckedOut, "Created book should not be checked out")
        
    }
    
    func testBookFromAPIWithWrongData() {
        // When 
        book = Book(json: BookHelper.allNilJSONdata())
        
        // Then
        XCTAssertNotNil(book, "Book should still be created")
        XCTAssertNil(book.title, "Title should be nil")
    }

}