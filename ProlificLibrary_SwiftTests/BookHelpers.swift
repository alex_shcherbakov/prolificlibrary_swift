//
//  BookHelpers.swift
//  ProlificLibrary_Swift
//
//  Created by Alexander Shcherbakov on 1/14/16.
//  Copyright © 2016 Oleksandr Shcherbakov. All rights reserved.
//

import Foundation
import SwiftyJSON

internal class BookHelper {
    
    class func fakeJSONwithoutCheckout() -> JSON {
        return JSON([
            "author": "Ash Maurya",
            "categories": "process",
            "lastCheckedOut": NSNull(),
            "lastCheckedOutBy": NSNull(),
            "publisher": "O'REILLY",
            "title": "Running Lean",
            "url": "/books/1"])
    }
    
    class func fakeJSONcheckedOut() -> JSON {
        return JSON([
            "author": "Ash Maurya",
            "categories": "process",
            "lastCheckedOut": "2014-10-1 19:16:11",
            "lastCheckedOutBy": "Prolific Pablo",
            "publisher": "O'REILLY",
            "title": "Running Lean",
            "url": "/books/1"])
    }
    
    class func allNilJSONdata() -> JSON {
        return JSON([
            "author": NSNull(),
            "categories": NSNull(),
            "lastCheckedOut": NSNull(),
            "lastCheckedOutBy": NSNull(),
            "publisher": NSNull(),
            "title": NSNull(),
            "url": NSNull()])
    }
}