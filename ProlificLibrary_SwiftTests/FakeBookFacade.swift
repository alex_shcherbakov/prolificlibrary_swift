//
//  FakeBookFacade.swift
//  ProlificLibrary_Swift
//
//  Created by Alexander Shcherbakov on 1/15/16.
//  Copyright © 2016 Oleksandr Shcherbakov. All rights reserved.
//

import Foundation
@testable import ProlificLibrary_Swift

class FakeBookFacade: BookFacadeProtocol {
    
    var success = true
    var shouldFinishCall = true
    var getAvailableBooksWasCalled = false
    
    func getAvailableBooks(completion completion: (books: [Book], success: Bool) -> Void) {
        getAvailableBooksWasCalled = true
        
        if shouldFinishCall {
            completion(books: [Book()], success: success)
        }
    }
    
    func insertBookInLibrary(
        book book: Book,
        completion: (book: Book?, success: Bool) -> Void) {
            // fake behaviour
    }
    
    func editBook(
        book: Book,
        completion: (book: Book?, success: Bool) -> Void) {
            // fake behaviour
    }
    
    func checkoutBook(
        book: Book,
        forUser userName: String,
        completion: (book: Book?, success: Bool) -> Void) {
            // fake behaviour
    }
    
    func deleteBook(
        book: Book,
        completion: (success: Bool) -> Void) {
            // fake behaviour
    }
    
    func clearAllBooksWithCompletion(completion: (success: Bool) -> Void) { }
}