//
//  BookSpec.swift
//  ProlificLibrary_Swift
//
//  Created by Alexander Shcherbakov on 1/13/16.
//  Copyright © 2016 Oleksandr Shcherbakov. All rights reserved.
//

import Quick
import Nimble
import SwiftyJSON
@testable import ProlificLibrary_Swift

class BookSpec: QuickSpec {
    override func spec() {
        describe("a book") {
            
            var book: Book!
            
            context("is created by user") {
                beforeEach {
                    book = Book(title: "Legacy code", categories: "iOS",
                        publisher: "Robert C. Martin", author: "Michael C. Feathers")
                }
                
                it("Contains author, publisher, title and categorie") {
                    expect(book.author).to(match("Michael C. Feathers"))
                    expect(book.publisher).to(match("Robert C. Martin"))
                    expect(book.title).to(beTruthy())
                    expect(book.categories).to(beTruthy())
                }
                
                it("Does not contain other properties filled") {
                    expect(book.lastCheckedOut).to(beFalsy())
                    expect(book.lastCheckedOutBy).to(beFalsy())
                    expect(book.url).to(beNil())
                }
            }
            
            context("is created in result of API call") {
                
                beforeEach {
                    book = Book(json: BookHelper.fakeJSONcheckedOut())
                }
                
                it("contains url") {
                    expect(book.url).to(match("/books/1"))
                }
                
                it("does not contain lastCheckedOut") {
                    expect(book.lastCheckedOut).to(beTruthy())
                }
            }
            
            context("has wrong JSON data") {
                it("jjdjdj") {
                    
                }
            }
        }
    }
}