//
//  NSObject+BBAdditions.swift
//  BaubleBar
//
//  Created by Quentin Ribierre on 9/20/15.
//  Copyright © 2015 Prolific Interactive. All rights reserved.
//

import Foundation

// MARK: - NSObject extension to add helper methods
public extension NSObject{
    
    /// Class helper variable to return the name of the class without the namespace
    // inspired of http://stackoverflow.com/a/25128993
    public class var className: String{
        return NSStringFromClass(self).componentsSeparatedByString(".").last!
    }
    
    /// Helper variable to return the name of the class without the namespace
    public var className: String{
        return NSStringFromClass(self.dynamicType).componentsSeparatedByString(".").last!
    }
}
