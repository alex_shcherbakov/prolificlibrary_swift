//
//  BookTableCell.h
//  ProlificLibrary_Swift
//
//  Created by Alexander Shcherbakov on 10/12/15.
//  Copyright © 2015 Oleksandr Shcherbakov. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Book;

/**
 *  UITableViewCell subclass to display info from book
 */
@interface BookTableCell : UITableViewCell

/**
 *  Setup method to assign book instance that will be displayed to a cell
 *
 *  @param book Book object to display
 */
- (void)setupWithBook:(Book *)book;

@end
