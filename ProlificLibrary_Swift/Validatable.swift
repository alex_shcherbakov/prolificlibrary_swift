//
//  Validatable.swift
//  ProlificLibrary_Swift
//
//  Created by Alexander Shcherbakov on 3/10/16.
//  Copyright © 2016 Oleksandr Shcherbakov. All rights reserved.
//

import Foundation

protocol Validatable {
    var entryType: EntryType { get }
}

extension Validatable where Self: UITextField {
    func hasValidEntry() -> Bool {
        if let regEx = entryType.regEx(), text = text {
            let entryTest = NSPredicate(format: "SELF MATCHES %@", regEx)
            return entryTest.evaluateWithObject(text)
        }
        return false
    }
}

class EmailField: UITextField, Validatable {
    let entryType: EntryType = .Email
}

class PasswordField: UITextField, Validatable {
    let entryType: EntryType = .Password
}


