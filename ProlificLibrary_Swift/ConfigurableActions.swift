//
//  ConfigurableActions.swift
//  ProlificLibrary_Swift
//
//  Created by Alexander Shcherbakov on 3/18/16.
//  Copyright © 2016 Oleksandr Shcherbakov. All rights reserved.
//

import Foundation

/**
 *  Protocol for creating UIAlertActions
 */
protocol ConfigurableAction {
    static func action(withCompletion completion: ((UIAlertAction) -> Void)?) -> UIAlertAction
}

class OkAction: UIAlertAction, ConfigurableAction {
    static func action(withCompletion completion: ((UIAlertAction) -> Void)?) -> UIAlertAction {
        return UIAlertAction(
            title: "OK",
            style: .Default,
            handler: completion)
    }
}

class CancelAction: UIAlertAction, ConfigurableAction {
    static func action(withCompletion completion: ((UIAlertAction) -> Void)?) -> UIAlertAction {
        return UIAlertAction(
            title: "Cancel",
            style: .Default,
            handler: completion)
    }
}

class YesAction: UIAlertAction, ConfigurableAction {
    static func action(withCompletion completion: ((UIAlertAction) -> Void)?) -> UIAlertAction {
        return UIAlertAction(
            title: "YES",
            style: .Destructive,
            handler: completion)
    }
}