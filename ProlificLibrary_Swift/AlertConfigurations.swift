//
//  AlertConfigurations.swift
//  ProlificLibrary_Swift
//
//  Created by Alexander Shcherbakov on 3/18/16.
//  Copyright © 2016 Oleksandr Shcherbakov. All rights reserved.
//

import Foundation

/**
 *  Protocol for a struct that contains pre-defained variables
 */
protocol AlertConfiguration {
    var title: String { get }
    var message: String { get }
    var preferedStyle: UIAlertControllerStyle { get }
    var alertActions: [UIAlertAction] { get }
}

// MARK: - Alert Controllers

struct SuccessAlertConfiguration: AlertConfiguration {
    let title = "Success!"
    let message = "We're good to go!"
    let preferedStyle: UIAlertControllerStyle = .Alert
    var alertActions: [UIAlertAction]
    
    init(withActions actions: [((UIAlertAction) -> Void)?]) {
        let successAction = UIAlertAction(
            title: "Yey!",
            style: .Default,
            handler: actions[0])
        alertActions = [successAction]
    }
}

struct FailureAlertConfiguration: AlertConfiguration {
    let title = "Oh no!"
    let message = "Something went wrong..."
    let preferedStyle: UIAlertControllerStyle = .Alert
    var alertActions: [UIAlertAction]
    
    init(withActions actions: [((UIAlertAction) -> Void)?]) {
        let cancelAction = CancelAction.action(withCompletion: actions[0])
        alertActions = [cancelAction]
    }
}

struct WarningAlertConfiguration: AlertConfiguration {
    let title = "Warning!"
    let message = "Connection is lost! Try later!"
    let preferedStyle: UIAlertControllerStyle = .Alert
    var alertActions: [UIAlertAction]
    
    init(withActions actions: [((UIAlertAction) -> Void)?]) {
        let cancelAction = CancelAction.action(withCompletion: actions[0])
        alertActions = [cancelAction]
    }
}