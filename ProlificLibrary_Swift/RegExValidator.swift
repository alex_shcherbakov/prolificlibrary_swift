//
//  RegExValidator.swift
//  ProlificLibrary_Swift
//
//  Created by Alexander Shcherbakov on 3/15/16.
//  Copyright © 2016 Oleksandr Shcherbakov. All rights reserved.
//

import Foundation

/**
 *  Validates text entry using specific regEx. Conforms to EntryValidator
 */
struct RegExValidator: EntryValidator {
    func isEntryValid(text text: String?, forEntryType entryType: EntryType) -> Bool {
        if let regEx = entryType.regEx(), text = text {
            let entryTest = NSPredicate(format: "SELF MATCHES %@", regEx)
            return entryTest.evaluateWithObject(text)
        }
        return false
    }
}