//
//  BookFacede.swift
//  ProlificLibrary_Swift
//
//  Created by Alexander Shcherbakov on 9/7/15.
//  Copyright © 2015 Oleksandr Shcherbakov. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

protocol BookFacadeProtocol {
    func getAvailableBooks(completion completion: (books: [Book], success: Bool) -> Void)
    func insertBookInLibrary(book book: Book, completion: (book: Book?, success: Bool) -> Void)
    func editBook(book: Book, completion: (book: Book?, success: Bool) -> Void)
    func checkoutBook(book: Book, forUser userName: String, completion: (book: Book?, success: Bool) -> Void)
    func deleteBook(book: Book, completion: (success: Bool) -> Void)
    func clearAllBooksWithCompletion(completion: (success: Bool) -> Void)
}

/// Facade class to comunicate with library API
public class BookFacade: BookFacadeProtocol {
    
    private struct Constants {
        static let libraryURL = "http://prolific-interview.herokuapp.com/560d83dd09dd0a000940d357"
        static let getAllBooksEndpoint = "/books"
        static let bookEndpoint = "/books/"
        static let clearBooksEndpoint = "/clean"
        static let lastCheckedOutByKey = "lastCheckedOutBy"
        static let authorKey = "author"
        static let categoriesKey = "categories"
        static let titleKey = "title"
        static let publisherKey = "publisher"
    }
    
    /**
    Method to get all books from library
    
    - parameter completion: closure with a book instance
    */
    public func getAvailableBooks(completion completion: (books: [Book], success: Bool) -> Void) {
        let stringURL = Constants.libraryURL + Constants.getAllBooksEndpoint
        
        Alamofire.request(.GET, stringURL).response { (_, _, data, _) -> Void in
            let json = JSON(data: data!)
            var library = [Book]()
            for (_, subjason) in json {
                library.append(Book(json: subjason))
            }
            completion(books: library, success: true)
        }
    }
    
    /**
    Method to insert given book to a library
    
    - parameter book:       Book instance to insert
    - parameter completion: Closure with book instance
    */
    public func insertBookInLibrary(book book: Book, completion: (book: Book?, success: Bool) -> Void) {
        let stringURL = Constants.libraryURL + Constants.bookEndpoint
        Alamofire
            .request(.POST, stringURL,
                parameters: self.JSONDictionaryFromBook(book),
                encoding: .JSON,
                headers: nil)
            .response { (_, _, data, _) -> Void in
                let json = JSON(data: data!)
                completion(book: Book(json: json), success: true)
            }
    }
    
    /**
    Method to edit given book in a library
    
    - parameter book:       Book instance to edit
    - parameter completion: Closure with a book instance
    */
    public func editBook(book: Book, completion: (book: Book?, success: Bool) -> Void) {
        let stringURL = Constants.libraryURL + book.url!
        Alamofire
            .request(.PUT, stringURL,
                parameters: JSONDictionaryFromBook(book),
                encoding: .JSON,
                headers: nil)
            .response {
                (_, _, data, _) -> Void in
                let json = JSON(data: data!)
                completion(book: Book(json: json), success: true)
        }
    }
    
    /**
    Method to track the last person who checked out the book
    
    - parameter book:       Book instance that is checked out
    - parameter userName:   String name of a person
    - parameter completion: Closure with a book instance
    */
    public func checkoutBook(book: Book, forUser userName: String,
        completion: (book: Book?, success: Bool) -> Void) {
        let stringURL = Constants.libraryURL + book.url!
        Alamofire
            .request(.PUT, stringURL,
                parameters: [ Constants.lastCheckedOutByKey:userName ],
                encoding: .JSON,
                headers: nil)
            .response { (_, _, data, _) -> Void in
                let json = JSON(data: data!)
                completion(book: Book(json: json), success: true)
            }
    }
    
    /**
    Method to delete given book from library
    
    - parameter book:       Book instance to be deleted
    - parameter completion: Closure with a book instance
    */
    public func deleteBook(book: Book, completion: (success: Bool) -> Void) {
        let stringURL = Constants.libraryURL + book.url!
        let headers = [ "Content-Type": "application/x-www-form-urlencoded" ]
        Alamofire.request(.DELETE, stringURL, encoding: ParameterEncoding.JSON, headers: headers).response {
            (_, _, _, error) -> Void in
            completion(success: error == nil)
        }
    }
    
    /**
    Method to delete all book from a library
    
    - parameter completion: Closure with a book instance
    */
    public func clearAllBooksWithCompletion(completion: (success: Bool) -> Void) {
        let stringURL = Constants.libraryURL + Constants.clearBooksEndpoint
        Alamofire.request(.DELETE, stringURL).response {
            (_, _, _, error) -> Void in
            completion(success: error == nil)
        }
    }
    
    private func JSONDictionaryFromBook(book: Book) -> [String : AnyObject]? {
        return [
            Constants.authorKey     : book.author,
            Constants.categoriesKey : book.categories,
            Constants.titleKey      : book.title,
            Constants.publisherKey  : book.publisher
        ]
    }
}