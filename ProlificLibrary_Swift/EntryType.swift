//
//  EntryType.swift
//  ProlificLibrary_Swift
//
//  Created by Oleksandr Shcherbakov on 3/13/16.
//  Copyright © 2016 Oleksandr Shcherbakov. All rights reserved.
//

import Foundation

enum EntryType {
    case Email
    case Password
    case State
    case Address
    case PhoneNumber
    case PostalCode
    
    
    func regEx() -> String? {
        switch self {
        case .Email:
            return "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        case .Password:
            return "[A-Z0-9a-z._%+-:/><#]{6,30}"
        default:
            return nil
        }
    }
}