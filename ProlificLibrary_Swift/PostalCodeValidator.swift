//
//  PostalCodeValidator.swift
//  ProlificLibrary_Swift
//
//  Created by Alexander Shcherbakov on 3/15/16.
//  Copyright © 2016 Oleksandr Shcherbakov. All rights reserved.
//

import Foundation

/**
 *  Evaluates postal codes (yes, not everybody has a ZIP code)
 */
struct PostalCodeValidator: EntryValidator {
    func isEntryValid(text text: String?, forEntryType entryType: EntryType) -> Bool {
        if entryType == .PostalCode {
            // validate code depending on country
            return true
        }
        return false
    }
}