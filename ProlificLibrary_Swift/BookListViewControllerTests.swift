//
//  BookListViewControllerXCTests.swift
//  ProlificLibrary_Swift
//
//  Created by Alexander Shcherbakov on 1/15/16.
//  Copyright © 2016 Oleksandr Shcherbakov. All rights reserved.
//

import XCTest
@testable import ProlificLibrary_Swift

class BookListViewControllerXCTests: XCTestCase {

    var sut: BookListViewController!
    var fakeFacade: FakeBookFacade!
    
    override func setUp() {
        super.setUp()
        sut = UIStoryboard(name: "Main", bundle: nil)
            .instantiateViewControllerWithIdentifier("BookListViewController") as? BookListViewController
        fakeFacade = FakeBookFacade()
        sut.bookFacade = fakeFacade
    }
    
    override func tearDown() {
        sut = nil
        fakeFacade = nil
        super.tearDown()
    }
    
    func test_views_are_hidden_when_data_is_not_loaded() {
        // given
        fakeFacade.shouldFinishCall = false
        
        // when
        let _ = sut.view // call viewDidLoad
        
        // then
        XCTAssertTrue(sut.tableView.hidden,
            "it should hide tableview")
        XCTAssertTrue(sut.activityIndicator.isAnimating(),
            "it should hide activity indicator")
    }

    func test_view_displays_data_when_data_is_loaded() {
        // given
        fakeFacade.shouldFinishCall = true
        fakeFacade.success = true
        
        // when
        let _ = sut.view
        let numberOfRows = sut.tableView(sut.tableView, numberOfRowsInSection: 0)
        
        // than
        XCTAssertTrue(fakeFacade.getAvailableBooksWasCalled,
            "data call should be made")
        XCTAssertFalse(sut.activityIndicator.isAnimating(),
            "activity indicator should disappear")
        XCTAssertFalse(sut.tableView.hidden,
            "table view should be on screen")
        XCTAssertEqual(numberOfRows, sut.books?.count,
            "number of rows should be equal to number of books")
    }
}
