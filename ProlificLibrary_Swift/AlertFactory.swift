//
//  AlertFactory.swift
//  ProlificLibrary_Swift
//
//  Created by Alexander Shcherbakov on 3/17/16.
//  Copyright © 2016 Oleksandr Shcherbakov. All rights reserved.
//

import Foundation

enum AlertType {
    case Success
    case Failure
}

struct AlertFactory {
    
    typealias ActionHandler = ((UIAlertAction) -> Void)?
    
    static func createAlertController(
        ofType type: AlertType,
        actions: [ActionHandler]) -> UIAlertController? {
            let settings = settingsForAlertType(type, handlers: actions)
            let alertController = UIAlertController(
                title: settings.title,
                message: settings.message,
                preferredStyle: settings.preferedStyle)
            
            for action in settings.alertActions {
                alertController.addAction(action)
            }
            
            return alertController
    }
    
    private static func settingsForAlertType(type: AlertType, handlers: [ActionHandler]) -> AlertConfiguration {
        switch type {
        case .Success:
            return SuccessAlertConfiguration(withActions: handlers)
        case .Failure:
            return FailureAlertConfiguration(withActions: handlers)
        }
    }
}





