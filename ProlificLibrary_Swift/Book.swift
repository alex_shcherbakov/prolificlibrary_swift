//
//  Book.swift
//  ProlificLibrary_Swift
//
//  Created by Oleksandr Shcherbakov on 9/6/15.
//  Copyright © 2015 Oleksandr Shcherbakov. All rights reserved.
//

import Foundation
import SwiftyJSON


protocol Readable {
    var author: String! { get }
    var categories: String! { get }
    var lastCheckedOut: String? { get }
    var lastCheckedOutBy: String? { get }
    var publisher: String! { get }
    var title: String! { get }
    var url: String? { get }
    var type: BookType? { get }
}


/// Class to describe a Book object
public class Book: NSObject, Readable {
    var author: String!
    var categories: String!
    var lastCheckedOut: String?
    var lastCheckedOutBy: String?
    var publisher: String!
    var title: String!
    var url: String?
    var type: BookType?
    
    /**
    Convenience initializer for creating book from existing data
    
    - parameter title:      String title
    - parameter categories: String categories
    - parameter publisher:  String publisher
    - parameter author:     String author
    
    - returns: Book instance
    */
    public convenience init(title: String, categories: String, publisher: String, author: String) {
        self.init()
        self.author = author
        self.categories = categories
        self.publisher = publisher
        self.title = title
    }
    
    /**
    Convenience initializer for JSON parsing
    
    - parameter json: JSON struct
    
    - returns: Book instance
    */
    public convenience init(json: JSON) {
        self.init()
        self.author = json["author"].string
        self.categories = json["categories"].string
        self.lastCheckedOutBy = json["lastCheckedOutBy"].string
        self.lastCheckedOut = json["lastCheckedOut"].string
        self.url = json["url"].string
        self.publisher = json["publisher"].string
        self.title = json["title"].string
    }
}