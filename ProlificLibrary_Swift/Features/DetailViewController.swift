//
//  DetailViewController.swift
//  ProlificLibrary_Swift
//
//  Created by Oleksandr Shcherbakov on 9/6/15.
//  Copyright © 2015 Oleksandr Shcherbakov. All rights reserved.
//

import UIKit

/**
* Delegate used to update the state of a presented book
*/
@objc protocol DetailViewControllerDelegate {
    
    /**
    Method to notify delegate that a book has been checked out
    
    - parameter book: Book object that contains updated info
    */
    func detailViewControllerDidCheckoutBook(book: Book?)
}

class DetailViewController: UIViewController {
    
    private struct Constants {
        static let identifyAlertTitle   = "Identify yourself"
        static let identifyAlertMessage = "Please enter your name"
    }
    
    // MARK: - Properties
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var authorLabel: UILabel!
    @IBOutlet private weak var publisherLabel: UILabel!
    @IBOutlet private weak var tagsLabel: UILabel!
    @IBOutlet private weak var lastCheckedOutTitleLabel: UILabel!
    @IBOutlet private weak var lastCheckedOutByLabel: UILabel!
    @IBOutlet private weak var shadingView: UIView!
    @IBOutlet private weak var spinner: UIActivityIndicatorView!
    private var book: Book?
    private var delegate: DetailViewControllerDelegate?
    
    private lazy var editBookViewController: EditBookViewController? = {
        if let controller = self.storyboard?
            .instantiateViewControllerWithIdentifier(EditBookViewController.className)
            as? EditBookViewController {
                return controller
        }
        return nil
    }()
    
    private lazy var loginViewController: LoginViewController? = {
        if let controller = self.storyboard?
            .instantiateViewControllerWithIdentifier(LoginViewController.className)
            as? LoginViewController {
                return controller
        }
        return nil
    }()
    
    // MARK: - View Controller Lifecycle
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setupDesign()
    }
    
    // MARK: - Setup Methods
    
    func setup(book book: Book?, delegate: DetailViewControllerDelegate) {
        self.book = book
        self.delegate = delegate
    }
    
    private func setupDesign() {
        self.lastCheckedOutTitleLabel.hidden = true
        self.lastCheckedOutByLabel.hidden = true
        self.spinner.hidesWhenStopped = true
        self.shadingView.hidden = true
        
        if let bookToDisplay = self.book {
            self.titleLabel.text = bookToDisplay.title
            self.authorLabel.text = bookToDisplay.author
            self.publisherLabel.text = bookToDisplay.publisher
            self.setupCategoriesLabelWithBook(bookToDisplay)
            
            if let checkedOutString = self.checkOutBlameStringForBook(bookToDisplay) {
                self.lastCheckedOutTitleLabel.hidden = false
                self.lastCheckedOutByLabel.hidden = false
                self.lastCheckedOutByLabel.text = checkedOutString
            }
        }
    }
    
    // MARK: - Action Methods
    
    @IBAction func checkoutButtonTapped(sender: AnyObject) {
        if let registerViewController = loginViewController {
            registerViewController.delegate = self
            presentViewController(registerViewController, animated: true, completion: nil)
        }
    }
    
    @IBAction func editButtonTapped(sender: AnyObject) {
        if let editController = self.editBookViewController {
            if let presentingVC = self.navigationController?.viewControllers.first
                as? BookListViewController {
                    editController.setupWithBook(self.book, delegate: presentingVC)
                    self.presentViewController(editController, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func shareButtonTapped(sender: AnyObject) {
        if let url = NSURL(string: self.book!.url!) {
            let activityVC = UIActivityViewController(
                activityItems: ["Some text here...", url],
                applicationActivities: nil)
            self.presentViewController(activityVC,
                animated: true,
                completion: nil)
        }
    }
    
    // MARK: - Private Methods
    
    private func checkoutBook(book: Book, user: String) {
        BookFacade().checkoutBook(self.book!, forUser: user) {
            [weak self] (book, success) -> Void in
            if success {
                self?.book = book
                self?.setupDesign()
                self?.presentAlertOnCheckout(book)
                self?.delegate?.detailViewControllerDidCheckoutBook(book)
            }
        }
    }
    
    private func presentAlertOnCheckout(book: Book?) {
        let title = (book != nil) ? "Success!" : "Failure"
        let message = (book != nil) ?
            "Book was checked out on \(book!.lastCheckedOut!)" : "Something went wrong"
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: {
            [weak self] (action) -> Void in
            self?.navigationController?.popToRootViewControllerAnimated(true)
        }))
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    private func setupCategoriesLabelWithBook(book: Book?) {
        if let categories = book?.categories {
            if categories.characters.count > 0 {
                self.tagsLabel.text = categories
            } else {
                self.tagsLabel.hidden = true
            }
        }
    }
    
    private func checkOutBlameStringForBook(book: Book?) -> String? {
        var blameString = ""
        if let personString = book?.lastCheckedOutBy {
            blameString = blameString + personString
        }
        if let dateString = book?.lastCheckedOut {
            blameString = blameString + ", " + dateString
        }
        if blameString.characters.count > 0 {
            return blameString
        }
        return nil
    }
}

extension DetailViewController: LoginProtocol {
    func userDidLogin(withEmail email: String?) {
        if let email = email, book = book {
            checkoutBook(book, user: email)
        }
    }
}
