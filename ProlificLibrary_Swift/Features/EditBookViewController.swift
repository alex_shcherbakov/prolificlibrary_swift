//
//  EditBookViewController.swift
//  ProlificLibrary_Swift
//
//  Created by Oleksandr Shcherbakov on 9/6/15.
//  Copyright © 2015 Oleksandr Shcherbakov. All rights reserved.
//

import UIKit

/**
*  Protocole used to notify a delegate about chenges to a book
*/
@objc protocol EditBookViewControllerDelegate {
    
    /**
    Method to notify a delegete that a book has been deleted
    
    - parameter book: Book object that was removed from server
    */
    func editBookViewControllerDidDeleteBook(book: Book)
    
    /**
    Method to notify a delegate that a book was added
    
    - parameter book: Book object returned from server
    */
    func editBookViewControllerDidAddBook(book: Book)
}

/// UIViewController subclass to create or edit a Book object
class EditBookViewController: UIViewController {
    
    private struct Constants {
        static let successAlertTitle       = "Success!"
        static let failureAlertTitle       = "Oh no!"
        static let warningAlertTitle       = "Warning!"
        static let bookTitleTextfieldName  = "book title"
        static let authorNameTextfieldName = "author name"
        static let categoryTextfieldName   = "book category"
        static let publisherTextfieldName  = "publisher name"
        static let defaultAlertActionTitle = "Let's fix it!"
        static let confirmationActionText  = "Are you sure you want to discard changes?"
        static let deletionWarning         = "Are you sure you want to delete this book?"
    }
    
    // MARK: - Properties
    
    @IBOutlet private weak var shadowView: UIView!
    @IBOutlet private weak var bookTitleTextfield: UITextField!
    @IBOutlet private weak var authorTextField: UITextField!
    @IBOutlet private weak var publisherTextfield: UITextField!
    @IBOutlet private weak var categoryTextfield: UITextField!
    @IBOutlet private weak var spinner: UIActivityIndicatorView!
    private var book: Book?
    private var delegate: EditBookViewControllerDelegate?
    
    private lazy var bookFacade: BookFacade = {
        return BookFacade()
    }()
    
    // MARK: View Controller Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupDesign()
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        presentNewAction()

    }
    
    // MARK: Setup Methods
    
    func setupDesign() {
        self.spinner.hidesWhenStopped = true
        self.shadowView.hidden = true
        
        if let book = self.book {
            self.bookTitleTextfield.text = book.title
            self.authorTextField.text = book.author
            self.publisherTextfield.text = book.publisher
            self.categoryTextfield.text = book.categories
        }
    }
    
    func setupWithBook(book: Book?, delegate: EditBookViewControllerDelegate) {
        self.book = book
        self.delegate = delegate
    }
    
    // MARK: Action Methods
    
    @IBAction func doneButtonTapped(sender: AnyObject) {
        if !dataDidChange() {
            self.dismissViewControllerAnimated(true, completion: nil)
        } else {
            self.askUserIfHeWantsToDiscartChanges()
        }
    }
    
    @IBAction func deleteButtonTapped(sender: AnyObject) {
        self.presentAlertToConfirmDeletion()
    }
    
    @IBAction func submitButtonTapped(sender: AnyObject) {
        if allTextfieldsFilled() {
            if let bookToEdit = self.book { // update existing book
                self.editBook(bookToEdit)
            } else {
                // we can manually unwrap optionals becouse all textfields are required to fill
                self.loadBook(title: self.bookTitleTextfield.text!,
                    categories: self.categoryTextfield.text!,
                    publisher: self.publisherTextfield.text!,
                    author: self.authorTextField.text!)
            }
        } else {
            self.presentAlertWithWarning()
        }
    }
    
    // MARK: - Private Methods
    
    private func allTextfieldsFilled() -> Bool {
        return
            self.bookTitleTextfield.hasText() &&
            self.authorTextField.hasText() &&
            self.publisherTextfield.hasText() &&
            self.categoryTextfield.hasText()
    }
    
    private func dataDidChange() -> Bool {
        if self.book != nil {
            return self.didChangeDataForBook(self.book)
        } else {
            return self.didEnterNewData()
        }
    }
    
    private func didChangeDataForBook(book: Book?) -> Bool {
        if let bookToCheck = book {
            return
                bookToCheck.title != self.bookTitleTextfield.text ||
                bookToCheck.author != self.authorTextField.text ||
                bookToCheck.categories != self.categoryTextfield.text ||
                bookToCheck.publisher != self.publisherTextfield.text
        } else {
            return false
        }
    }
    
    private func didEnterNewData() -> Bool {
        return
            self.bookTitleTextfield.hasText() ||
            self.authorTextField.hasText() ||
            self.categoryTextfield.hasText() ||
            self.publisherTextfield.hasText()
    }
    
    private func emptyTextFieldMessage() -> String {
        var textfieldName = ""
        for textField in [self.bookTitleTextfield, self.authorTextField,
                          self.publisherTextfield, self.categoryTextfield] {
            if !textField.hasText() {
                textfieldName = self.nameForTextfield(textField)
            }
        }
        return "Oops! You forgot to specify \(textfieldName). Please fill it up!"
    }
    
    private func nameForTextfield(textfield: UITextField) -> String {
        switch textfield.tag {
        case 1: return Constants.bookTitleTextfieldName
        case 2: return Constants.authorNameTextfieldName
        case 3: return Constants.publisherTextfieldName
        case 4: return Constants.categoryTextfieldName
        default: return ""
        }
    }
    
    // MARK: - Private Methods - API calls
    
    private func editBook(book: Book) {
        book.title = self.bookTitleTextfield.text
        book.categories = self.categoryTextfield.text
        book.publisher = self.publisherTextfield.text
        book.author = self.authorTextField.text
        
        self.spinner.startAnimating()
        self.bookFacade.editBook(book, completion: {
            [unowned self] (book, success) -> Void in
            if let returnedBook = book {
                self.spinner.stopAnimating()
                self.book = returnedBook
                self.delegate?.editBookViewControllerDidAddBook(returnedBook)
                self.presentSuccessMessage(book != nil, forBook: returnedBook)
            }
        })
    }
    
    private func loadBook(title title: String, categories: String,
        publisher: String, author: String) {
            let newBook = Book(title: title,
                categories: categories,
                publisher: publisher,
                author: author)
            
            self.shadowView.hidden = false
            self.spinner.startAnimating()
            self.bookFacade.insertBookInLibrary(book: newBook, completion: {
                [weak self] (book, success) -> Void in
                self?.shadowView.hidden = true
                self?.spinner.stopAnimating()
                
                if let returnedBook = book {
                    self?.book = returnedBook
                    self?.delegate?.editBookViewControllerDidAddBook(returnedBook)
                    self?.presentSuccessMessage(book != nil, forBook: returnedBook)
                }
            })
    }
    
    private func deleteBook(book: Book) {
        self.bookFacade.deleteBook(book) {
            [unowned self] (success) -> Void in
            self.delegate?.editBookViewControllerDidDeleteBook(self.book!)
            self.presentingViewController?
                .dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    // MARK: - Private Methods - Alerts
    
    private func presentSuccessMessage(success: Bool, forBook book: Book) {
        let message = success ? "We're good to go!" : "Something went wrong..."
        let alertTitle = success ? Constants.successAlertTitle : Constants.failureAlertTitle
        let alertController = UIAlertController(title: alertTitle,
            message: message, preferredStyle: .Alert)
        alertController.addAction(self.actionForSuccessMessage(success))
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    private func actionForSuccessMessage(success: Bool) -> UIAlertAction {
        let alertTitle = success ? Constants.successAlertTitle : Constants.failureAlertTitle
        
        let successAction = (UIAlertAction(title: alertTitle, style: .Default, handler: {
            [unowned self] (_) -> Void in
            self.dismissViewControllerAnimated(true, completion: nil)
            }))
        
        let failureAction = (UIAlertAction(title: alertTitle, style: .Default, handler: nil))
        return success ? successAction : failureAction
    }
    
    private func presentAlertWithWarning() {
        let alertController = UIAlertController(title: Constants.warningAlertTitle,
            message: self.emptyTextFieldMessage(), preferredStyle: .Alert)
        alertController.addAction(UIAlertAction(title: Constants.defaultAlertActionTitle,
            style: .Default, handler: nil))
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    private func presentAlertToConfirmDeletion() {
        let alertController = UIAlertController(title: Constants.warningAlertTitle,
            message: Constants.deletionWarning, preferredStyle: .Alert)
        alertController.addAction(UIAlertAction(title: "YES",
            style: .Destructive, handler: {
                [unowned self] (action) -> Void in
                if let book = self.book {
                    self.deleteBook(book)
                }
        }))
        alertController.addAction(UIAlertAction(title: "NO", style: .Default, handler: nil))
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    private func askUserIfHeWantsToDiscartChanges() {
        let alertController = UIAlertController(title: Constants.warningAlertTitle,
            message: Constants.confirmationActionText, preferredStyle: .Alert)
        alertController.addAction(UIAlertAction(title: "YES", style: .Destructive, handler: {
            [unowned self] (action) -> Void in
            self.dismissViewControllerAnimated(true, completion: nil)
            }))
        alertController.addAction(UIAlertAction(title: "NO", style: .Default, handler: nil))
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    private func presentNewAction() {
        let okHandler: ((UIAlertAction) -> Void)? = { action in
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        
        if let alertController = AlertFactory.createAlertController(ofType: .Success, actions: [okHandler]) {
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
}

// MARK: - UITextFieldDelegate

extension EditBookViewController : UITextFieldDelegate {
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
