//
//  BookListViewController.swift
//  ProlificLibrary_Swift
//
//  Created by Oleksandr Shcherbakov on 9/6/15.
//  Copyright © 2015 Oleksandr Shcherbakov. All rights reserved.
//

import UIKit
import Foundation
import Alamofire

/// UIViewController subclass to present list of books in library
class BookListViewController: UIViewController {
    
    private struct Constants {
        static let cellHeight: CGFloat = 60
        static let successTitle = "Success"
        static let successMessage = "Library is clear, you can add new books"
        static let failureTitle = "Failure"
        static let failureMessage = "Something went wrong. Try again later"
    }
    
    // MARK: - Properties
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var books: [Book]?
    var bookFacade: BookFacadeProtocol = BookFacade()
    
    // New way
    private lazy var bookDetailsViewController: DetailViewController? = {
        if let controller: DetailViewController = ConstollerFactory.createController() {
            return controller
        }
        return nil
    }()
    
    // Old way
    private lazy var editBookViewController: EditBookViewController? = {
        if let controller =
            UIStoryboard(name: "Main", bundle: nil)
            .instantiateViewControllerWithIdentifier("EditBookViewController")
            as? EditBookViewController {
            return controller
        }
        return nil
    }()
    
    //MARK: - ViewController Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupDesign()
        setupData()
    }
    
    //MARK: - Setup Methods
    
    private func setupDesign() {
        tableView.registerNib(UINib(nibName: BookTableCell.className, bundle: nil),
            forCellReuseIdentifier: BookTableCell.className)
        tableView.hidden = true
        activityIndicator.hidesWhenStopped = true
    }
    
    private func setupData() {
        activityIndicator.startAnimating()
        bookFacade.getAvailableBooks { [weak self] (books, success) -> Void in
            if success {
                self?.books = books
                self?.activityIndicator.stopAnimating()
                self?.tableView.hidden = false
                self?.tableView.reloadData()
            }
        }
    }
    
    // MARK: - Action Methods
    
    @IBAction func addNewButtonTapped(sender: AnyObject) {
        if let editVC = editBookViewController {
            editVC.setupWithBook(nil, delegate: self)
            presentViewController(editVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func deleteAllButtonTapped(sender: AnyObject) {
        bookFacade.clearAllBooksWithCompletion { [weak self] (success) -> Void in
            self?.presentAlertOnClearLibrary(success)
        }
    }
    
    // MARK: - Private Methods
    
    func presentAlertOnClearLibrary(success: Bool) {
        let controllerTitle = success ? Constants.successTitle : Constants.failureTitle
        let alertMessage = success ? Constants.successMessage : Constants.failureMessage
        let alertController = UIAlertController(title: controllerTitle,
            message: alertMessage, preferredStyle: .Alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
        presentViewController(alertController, animated: true) {
            [weak self] () -> Void in
            self?.books = nil
            self?.tableView.reloadData()
        }
    }
}

//MARK: - <UITableViewDateDelegate>

extension BookListViewController: UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let detailController = bookDetailsViewController {
            detailController.setup(book: books?[indexPath.row], delegate: self)
            navigationController?.pushViewController(detailController, animated: true)
        }
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
    }
}

//MARK: - <UITableViewDateSource>

extension BookListViewController: UITableViewDataSource {
    func tableView(tableView: UITableView, cellForRowAtIndexPath
        indexPath: NSIndexPath) -> UITableViewCell {
        if let cell = tableView
            .dequeueReusableCellWithIdentifier(BookTableCell.className) as? BookTableCell {
                if books?.count > 0 {
                    if let book = books?[indexPath.row] {
                        cell.setupWithBook(book)
                        return cell
                    }
                }
            }
        return UITableViewCell()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let number = books?.count {
            return number
        }
        return 0
    }
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath
        indexPath: NSIndexPath) -> CGFloat {
        return Constants.cellHeight
    }
}

// MARK: - <DetailViewControllerDelegate>

extension BookListViewController : DetailViewControllerDelegate {
    func detailViewControllerDidCheckoutBook(book: Book?) {
        if let editedBook = book {
            if let booksArray = books {
                for oldBook in booksArray {
                    if oldBook.url == editedBook.url {
                        oldBook.lastCheckedOut = editedBook.lastCheckedOut
                        oldBook.lastCheckedOutBy = editedBook.lastCheckedOutBy
                    }
                }
            }
        }
    }
}

// MARK: - <EditBookViewControllerDelegate>

extension BookListViewController : EditBookViewControllerDelegate {
    func editBookViewControllerDidAddBook(book: Book) {
        if let booksArray = books {
            var bookIsOld = false
            for oldBook in booksArray {
                if oldBook.url == book.url {
                    let index = books?.indexOf(oldBook)
                    books?.removeAtIndex(index!)
                    books?.insert(book, atIndex: index!)
                    bookIsOld = true
                }
            }
            
            if !bookIsOld {
                books?.insert(book, atIndex: 0)
            }
            
            tableView.reloadData()
        }
    }
    
    func editBookViewControllerDidDeleteBook(book: Book) {
        navigationController?.popToRootViewControllerAnimated(true)
        if var booksArray = books {
            booksArray.removeAtIndex(booksArray.indexOf(book)!)
            books = booksArray
            tableView.reloadData()
        }
    }
}