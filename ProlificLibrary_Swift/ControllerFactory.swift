//
//  ControllerFactory.swift
//  ProlificLibrary_Swift
//
//  Created by Alexander Shcherbakov on 3/16/16.
//  Copyright © 2016 Oleksandr Shcherbakov. All rights reserved.
//

import Foundation

protocol Instantiatable { }

extension UIViewController: Instantiatable { }

struct ConstollerFactory {
    static func createController<T: Instantiatable>() -> T? {
        guard let storyboard = storyboardForType(T) else {
            return nil
        }
        
        return storyboard.instantiateViewControllerWithIdentifier(String(T)) as? T
    }
    
    private static func storyboardForType<T>(type: T) -> UIStoryboard? {
        if let storyboardId = storyboardId(forType: type) {
            return UIStoryboard(name: storyboardId, bundle: nil)
        } else {
            return nil
        }
    }
    
    private static func storyboardId<T>(forType type: T) -> String? {
        switch type {
        case is DetailViewController.Type: return "Main"
        case is EditBookViewController.Type: return "Main"
        default: return nil
        }
    }
}