//
//  BookTableCell.m
//  ProlificLibrary_Swift
//
//  Created by Alexander Shcherbakov on 10/12/15.
//  Copyright © 2015 Oleksandr Shcherbakov. All rights reserved.
//

#import "BookTableCell.h"

#import "ProlificLibrary_Swift-Swift.h"

@interface BookTableCell ()

@property (nonatomic, weak) IBOutlet UILabel *bookTitleLabel;
@property (nonatomic, weak) IBOutlet UILabel *bookAuthorsLabel;

@property (nonatomic, strong) Book *book;

@end

@implementation BookTableCell

#pragma mark - Cell Lifecycle

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self setupDesign];
}

#pragma mark - Setup Methods

- (void)setupWithBook:(Book *)book
{
    self.book = book;
    [self setupDesign];
}

#pragma mark - Internal Methods

- (void)setupDesign
{
    if (self.book.title) {
        self.bookTitleLabel.text = self.book.title;
    }
    
    if (self.book.author) {
        self.bookAuthorsLabel.text = self.book.author;
    }
}

@end
