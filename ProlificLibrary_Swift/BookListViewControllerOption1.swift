//
//  BookListViewController_Scenario.swift
//  ProlificLibrary_Swift
//
//  Created by Alexander Shcherbakov on 1/20/16.
//  Copyright © 2016 Oleksandr Shcherbakov. All rights reserved.
//

import XCTest
@testable import ProlificLibrary_Swift

class BookListViewControllerSpec: XCTestCase {
    
    var sut: BookListViewController!
    var fakeFacade: FakeBookFacade!
    
    override func setUp() {
        super.setUp()
        sut = UIStoryboard(name: "Main", bundle: nil)
            .instantiateViewControllerWithIdentifier("BookListViewController") as? BookListViewController
        fakeFacade = FakeBookFacade()
        sut.bookFacade = fakeFacade
    }
    
    override func tearDown() {
        sut = nil
        fakeFacade = nil
        super.tearDown()
    }
}

class BookListViewController_when_data_is_not_loaded: BookListViewControllerSpec {
    
    override func setUp() {
        super.setUp()
        fakeFacade.shouldFinishCall = false
        let _ = sut.view // call viewDidLoad
    }
    
    func test_it_should_show_activity_indicator() {
        XCTAssertTrue(sut.activityIndicator.isAnimating())
    }
    
    func test_it_should_hide_tableView() {
        XCTAssertTrue(sut.tableView.hidden)
    }
}

class BookListViewController_when_data_is_loaded: BookListViewControllerSpec {
    var numberOfRows: Int!
    
    override func setUp() {
        super.setUp()
        
        let _ = sut.view // call viewDidLoad
        numberOfRows = sut.tableView(sut.tableView, numberOfRowsInSection: 0)
    }
    
    func test_it_should_set_successful_flag() {
        XCTAssertTrue(fakeFacade.getAvailableBooksWasCalled)
    }
    
    func test_it_should_hide_activity_indicator() {
        XCTAssertFalse(sut.activityIndicator.isAnimating())
    }
    
    func test_it_should_show_tableView() {
        XCTAssertFalse(sut.tableView.hidden)
    }
    
    func test_it_should_display_all_books() {
        XCTAssertEqual(numberOfRows, sut.books?.count)
    }
}

















/*

+ looks a lot like BDD (closest we can get)
+ allows to write requirements without implementation

- has very long class names
- can't run test suit for entire class (only per class)
- list of test cases is huge -> hard to navigate
- less readable because doesn't have highlighted descriptions
- assertions are less descriptive when they fail
- violates the way Apple recommends to write tests

? this way will potentially take longer time to execute -> much more tests, more times to run setUp() and tearDown()

*/