//
//  LoginViewController.swift
//  ProlificLibrary_Swift
//
//  Created by Alexander Shcherbakov on 3/10/16.
//  Copyright © 2016 Oleksandr Shcherbakov. All rights reserved.
//

import Foundation

protocol LoginProtocol {
    func userDidLogin(withEmail email: String?)
}

class LoginViewController : UIViewController {
    
    var delegate: LoginProtocol?

    @IBOutlet weak var emailField: EmailField!
    @IBOutlet weak var passwordField: PasswordField!
    @IBOutlet weak var postalField: PostalField!
    
    @IBAction func submitButtonTapped(sender: AnyObject) {
        regularMethodToValidate()
    }

    private func isTextValid(text: String?, forEntryType entryType: EntryType) -> Bool {
        if let regEx = entryType.regEx(), text = text {
            let entryTest = NSPredicate(format: "SELF MATCHES %@", regEx)
            return entryTest.evaluateWithObject(text)
        }
        return false
    }
    
    private func regularMethodToValidate() {
        let emailValid = isTextValid(emailField.text, forEntryType: .Email)
        let passValid = isTextValid(passwordField.text, forEntryType: .Password)
        
        if emailValid && passValid {
            delegate?.userDidLogin(withEmail: emailField.text)
            dismissViewControllerAnimated(true, completion: nil)
        } else {
            print("Data is invalid, enter correct data")
        }
    }
    
    private func strategyPatternValidation() {
        if emailField.hasValidEntry() && passwordField.hasValidEntry() && postalField.hasValidEntry(){
            delegate?.userDidLogin(withEmail: emailField.text)
            dismissViewControllerAnimated(true, completion: nil)
        } else {
            print("Data is invalid, enter correct data")
        }
    }
}