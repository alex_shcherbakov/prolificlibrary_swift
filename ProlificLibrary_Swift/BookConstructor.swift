//
//  BookConstructor.swift
//  ProlificLibrary_Swift
//
//  Created by Alexander Shcherbakov on 3/16/16.
//  Copyright © 2016 Oleksandr Shcherbakov. All rights reserved.
//

import Foundation

struct BookConstructor {
    func createBook(withType type: BookType) -> Readable {
        switch type {
        case .Kindle:
            return KindleBook()
        case .PDF:
            return PDFBook()
        case .PaperBack:
            return Book()
        }
    }
}

enum BookType {
    case PaperBack
    case Kindle
    case PDF
}

class KindleBook: Book {
    var onKindleDevice: String?
}

class PDFBook: Book {
    var dropboxURL: String?
}