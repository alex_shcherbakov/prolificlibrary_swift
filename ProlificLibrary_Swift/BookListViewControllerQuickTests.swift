//
//  BookListViewControllerTests.swift
//  ProlificLibrary_Swift
//
//  Created by Alexander Shcherbakov on 1/15/16.
//  Copyright © 2016 Oleksandr Shcherbakov. All rights reserved.
//

import Nimble
import Quick

@testable import ProlificLibrary_Swift

class BookListViewControllerTests: QuickSpec {
    override func spec() {
        
        var viewController: BookListViewController!
        
        describe("book list view controller") {
            
            beforeEach {
                viewController = UIStoryboard(name: "Main", bundle: nil)
                    .instantiateViewControllerWithIdentifier("BookListViewController") as? BookListViewController
            }
            
            describe("when the view is loaded") {
                
                context("and data call is in process") {
                    
                    beforeEach {
                        let facade = FakeBookFacade()
                        facade.shouldFinishCall = false
                        viewController.bookFacade = facade

                        let _ = viewController.view // calls viewDidLoad
                    }
                    
                    it("should hide tableview") {
                        expect(viewController.tableView.hidden).to(beTrue())
                    }
                    
                    it("should hide activity indicator") {
                        expect(viewController.activityIndicator.alpha) == 1.0
                    }
                }
                
                
                context("when the data call was successful") {
                    
                    beforeEach {
                        viewController.bookFacade = FakeBookFacade()
                        let _ = viewController.view // calls viewDidLoad
                    }
                    
                    it("should make an API call to load library") {
                        let facade = viewController.bookFacade as! FakeBookFacade
                        expect(facade.getAvailableBooksWasCalled).to(beTrue())
                    }
                    
                    it("should hide activity indicator") {
                        expect(viewController.activityIndicator.isAnimating()).to(beFalse())
                    }
                    
                    it("should show tableview") {
                        expect(viewController.tableView.hidden).to(beFalse())
                    }
                    
                    it("should display retrieved data in tableview") {
                        expect(viewController.tableView(viewController.tableView,
                            numberOfRowsInSection: 0)).to(equal(1))
                    }
                }
            }
        }
    }
}