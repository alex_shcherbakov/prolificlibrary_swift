//
//  ValidatableStrategy.swift
//  ProlificLibrary_Swift
//
//  Created by Alexander Shcherbakov on 3/15/16.
//  Copyright © 2016 Oleksandr Shcherbakov. All rights reserved.
//

import Foundation

protocol ValidatableStrategy {
    var entryType: EntryType { get }
    var validator: EntryValidator { get }
}

extension ValidatableStrategy where Self: UITextField {
    func hasValidEntry() -> Bool {
        return validator.isEntryValid(text: text, forEntryType: entryType) ?? false
    }
}

class PostalField: UITextField, ValidatableStrategy {
    let entryType: EntryType = .PostalCode
    let validator: EntryValidator = PostalCodeValidator() // IMPORTANTE: no switch statements!
}

class PhoneNumberField: UITextField, ValidatableStrategy {
    let entryType: EntryType = .PhoneNumber
    let validator: EntryValidator = RegExValidator()
}


//    func validator() -> EntryValidator? {
//        switch entryType {
//        case .Email, .Password:
//            return RegExValidator()
//        case .PostalCode:
//            return PostalCodeValidator()
//        default:
//            return nil
//        }
//    }