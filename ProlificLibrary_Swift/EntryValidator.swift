//
//  EntryValidator.swift
//  ProlificLibrary_Swift
//
//  Created by Alexander Shcherbakov on 3/15/16.
//  Copyright © 2016 Oleksandr Shcherbakov. All rights reserved.
//

import Foundation

protocol EntryValidator {
    /**
     Method that checks if text of certain emtry type is valid or not
     
     - parameter text:      optional String
     - parameter entryType: type of entry (email, password, zip code etc)
     
     - returns: true if text is valid for given type
     */
    func isEntryValid(text text: String?, forEntryType entryType: EntryType) -> Bool
}