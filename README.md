# README #

Swift test project

### CocoaPods ###

*Alamofire*

*SwiftyJSON*

I also used extension for NSObject that allows to use swift class name without project suffix. Created by Quentin Ribierre

### Swift / Objective-C ###

99% of the project is written in swift.
Objective-C is used only for BookTableCell (as required by task)

## Architecture ##

### BookList
...represents a starting point of the app. From here user can review, edit and delete books from library. Relies on protocols to update data - to avoid making API calls on each appearance. 

### BookDetails 
...allows to review a book info, proceed to editing or checkout a book. It also allows to share book info on social media or other communication apps.

### EditingBooks 
...is used for both creating and editing a book. User can also delete book from this controller.

Content:
1. [Project Setup](Documentation/Project_Setup.md)
2. [Networking](Documentation/Networking.md)
3. [Developer Notes](Documentation/DevNotes.md)
4. [Deprecated Features](Documentation/Deprecated_Features.md)
5. [Pods Used](Documentation/PodsUsed.md)