# To use Peter Gabriel script, you need to install it with this command line :
# sudo npm install -g peter-gabriel

# Variables to configure for your project
PROJ_FOLDER=./
SCRIPT_PATH=./Scripts
STAGING_BRANCH=staging
MASTER_BRANCH=master
DEVELOP_BRANCH=develop
PROJECT_NAME="BaubleBar"
PROJECT_COMPANY="BaubleBar"
CODE_FOLDER_NAME=BaubleBar/

# MASTER
SCHEME_NAME_MASTER=BaubleBar
CONFIG_NAME_MASTER=Release
SETTINGS_BUNDLE_MASTER=./BaubleBar/Supporting\ Files/Settings-BaubleBar.bundle/Root.plist
PLIST_MASTER=./BaubleBar/Supporting\ Files/BaubleBar-Info.plist

# STAGING
SCHEME_NAME_STAGING=BaubleBar
CONFIG_NAME_STAGING=LobsterCombo
SETTINGS_BUNDLE_STAGING=./BaubleBar/Supporting\ Files/Settings-LobsterCombo.bundle/Root.plist
HOCKEY_TOKEN_STAGING=c6843905207548ad928ebe983a40f870
PLIST_STAGING=./ProlificLibrary_Swift/Info.plist

build: build-master

build-master:
	cd ${PROJ_FOLDER}; pod update;
	cd ${PROJ_FOLDER}; ${SCRIPT_PATH}/create_build.sh ${SCHEME_NAME_MASTER} ${CONFIG_NAME_MASTER}

build-staging:
	cd ${PROJ_FOLDER}; pod update;
	cd ${PROJ_FOLDER}; ${SCRIPT_PATH}/create_build.sh ${SCHEME_NAME_STAGING} ${CONFIG_NAME_STAGING}

distribute-master: build-master
	cd ${PROJ_FOLDER}
	${SCRIPT_PATH}/distribute_build_hockey.sh ${HOCKEY_TOKEN_MASTER}
	${SCRIPT_PATH}/bump_version.sh ${PLIST_MASTER} ${SETTINGS_BUNDLE_MASTER}

bump-build-version:
	cd ${PROJ_FOLDER}
<<<<<<< HEAD
	${SCRIPT_PATH}/bump_version.sh ${PLIST_STAGING}
	git commit -am "Bump build version"
	git push origin HEAD:${STAGING_BRANCH}
=======
	git checkout ${DEVELOP_BRANCH}
	git pull
	${SCRIPT_PATH}/bump_version.sh ${PLIST_STAGING}
	git commit -am "Bump build version"
	git push origin HEAD:${DEVELOP_BRANCH}

test-bump-2:
	make bump-build-version
>>>>>>> bef5088a05f895afca1ba453135485e4ba4638c2

distribute-staging: build-staging
	cd ${PROJ_FOLDER}
	${SCRIPT_PATH}/distribute_build_hockey.sh ${HOCKEY_TOKEN_STAGING}
	${SCRIPT_PATH}/bump_version.sh ${PLIST_STAGING} ${SETTINGS_BUNDLE_STAGING}
	git commit -am "bump build version"
	git push origin HEAD:${DEVELOP_BRANCH}

documentation:
	jazzy
	open docs/index.html
	rm -rf ./build

clean-code:
	cd ${PROJ_FOLDER}
	${SCRIPT_PATH}/clean_code.sh ${CODE_FOLDER_NAME}

push-build:
	curl https://www.bitrise.io/app/87811b311487858b/build/start.json --data-urlencode 'payload={"hook_info":{"type":"bitrise","api_token":"zy76_Oqqy6DoEao2EBXtcw"},"build_params":{"branch":"staging"}}'

push-testflight:
	curl https://www.bitrise.io/app/1f76ed669e4d560a/build/start.json --data-urlencode 'payload={"hook_info":{"type":"bitrise","api_token":"9df3XMTTEfyNk8DHQNyeuw"},"build_params":{"branch":"master"}}'
