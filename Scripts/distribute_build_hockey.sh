#!/bin/sh

# $1: hockey API token, to be create from hockey in API section (don't use app ID)

#TODO: the notes are generated between 2 commits, using the last successful build as a reference. Should find a better way to do that, not relying on Jenkins folder strcture..
ipa distribute:hockeyapp \
	--token $1\
	--mandatory \
	--notes "$(git log --no-merges --pretty="%s" --since="`/usr/local/opt/coreutils/bin/gdate  -r ./../lastSuccessful/build.xml "+%F %T"`")" \
	--notify
