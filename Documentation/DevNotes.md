#### Kill Switch and Force Update
We use [Bellerophon](https://github.com/prolificinteractive/Bellerophon) for Kill Switch feature and [Harpy](https://github.com/ArtSabintsev/Harpy) for Force Update. Both are triggered from AMS.

#### Universal links
Alex and Ani have universal links setup, we are using the `AALinkManager` to handle different hosts, different type of universal links and the parsing of the links since in the apple-app-site-association file we didn't specify specific path (i.e : [ * ]) because the structure of A&A URLs are not ready to do so.

Universal Links is used for two type of links : A link which leads to a product and links which leads to category. So we are redirecting to the `SHOP` tab to displays a CDP or a PDP.

We had to add the `AALinkConstants` to known if it's a url that we should be able to open in the app or redirecting to Safari.

`AALinkRequest` is a enum which use case pattern matching based on the URL path components. We have different types of request :
* Collection Category
* Collection Product
* Category
* Category Product
* Subcategory
* Subcategory Product

#### Error Handling
Every API error should be handled by AAErrorHandler. AAErrorHandler checks to see if an error is API or session related and take appropriate action for those situations.

There are two ways to handle error:
* Show user a notification banner alert.
* In case data is not loaded yet, show `AAEmptyView` with `reload` button.

Known errors are defined in `AAErrorType` file.

#### Payment Option
The `AAPaymentOptionViewController` is a modal view controller which present different payment method options. For now, this view controller is deleted because we only have two different payment methods:

* Credit Card
* Paypal

Credit Card and Paypal are using the same checkout flow and need the same informations. This view controller could be re-used when Apple-pay will be in the app, we may want to have a different checkout flow. If Apple-pay need this modal again, you can retrieve it with delete/payment_options_modal tag.

#### Notifications
We are using [TSMessage](https://github.com/prolificinteractive/TSMessages) to display notifications.

There is 4 kind of notifications:

* Success
* Message
* Warning
* Error

We forked TSMessage to customize it a little bit.  (e.g., center the notification's text). Furthermore, TSMessage's appearance is also customizable by editing the `AANotificationDesigns.json` file.

#### NSLocalization
We want to keep the app localized.

When you add a String to the project, make to sure to do the following:

* Add the String in the `Localizable.strings` file, with `"YOUR_STRING_KEYWORD" = "YourString";`
* Call the string with `NSLocalizedString(@"YOUR_STRING_KEYWORD", @"Description of Your String");`

Make sure to also use localization for everything else (Date, Number, etc.).

#### AAButton usage
The AAButton class applies a transparency effect on the background color of the button when pressing down on it. On release the button background color is styled back to its originalBackgroundColor.

To ensure proper usage of AAButton:
 1. Set the AAButton originalBackgroundColor property to be that of the background color you wish the button to be.
 2. Set the optional AAButton highlightedBackgroundColor to be the background color you wish to display on the button when the button is pressed/highlighted. If this property is not set the backgroundColor applied to the button when pressed is the originalBackgroundColor with a 0.9f opacity.
 3. Do not set the backgroundColor property of the AAButton programmatically or via interface builder unless you really must. Setting the originalBackgroundColor also sets the backgroundColor property of the button so doing this isn't necessary.

#### AAAddToCartButton usage
The AAAddToCartButton class supports one of three modes: AddToCartButtonLabelModeAddToCart(default), AddToCartButtonLabelModeAvailableDate and AddToCartButtonLabelModeSoldOut set via the labelMode property.

When AddToCartButtonLabelModeAddToCart is set (the default) the bag button will function as pressable button that displays a price and "add to cart" text. It is expected to set the price label (addToCartPriceLabel) and addToCartLabel for any custom text that should appear next to price.

When AddToCartButtonLabelModeAvailableDate is set the button will work as non-pressable button (more like a label) that displays text in the form of "Available September 30, 2015" and will not display the price label or the addToCartLabel.

When AddToCartButtonLabelModeSoldOut is set the button will work as non-pressable button (more like a label) that displays the "SOLD OUT" text and will not display the price label or the addToCartLabel.

Be sure to call set the labelMode property (never the instance variable) when changing the labelMode of the AAAddToCartButton class for proper usage.

#### Navigation Title Kerning and setupDesign/setupNavigationBar
The design of this project requires a custom kerning of 2.5f applied to all Navigation bar titles but unfortunately this cannot be applied through the UIAppearance proxy of the NavigationBar. This means that any view controller which is to have a navigation bar title must:

1. Implement a setupNavigationBar method which is called from the setupDesign method.
2. In the setupNavigationBar method:
  • Set the view controller title (ex: self.title = NSLocalizedString(@"title here") and immediately after call [self applyDefaultNavBarStyle]. The applyDefaultNavBarStyle: method is a Category Method on UIViewController which will take care of the specifics on applying the default project font, font size and kerning to the navigation title. Note that whenever you change the title of the view controller and are expecting it to reflect in the navigation bar you must call the applyDefaultNavBarStyle: method. Feel free to inspect the UIViewController+AAAdditions.h for additional methods you may wish to use.
  • Apply any additional customizations regarding the navigationBar.
3. If you are subclassing from a parent class which uses its own setupDesign and setupNavigationBar methods do the following:
  • Call [self setupDesign]/[self setupNavigationBar] ONLY in the top most parent class' viewDidLoad method and make the two methods public so they can be called directly by subclasses. On all children of the parent class override the setupDesign to first call [super setupDesign] and setupNavigationBar to first call [super setupNavigationBar] methods. Then feel free to apply your custom stylings in the two methods. Note once again that in the children classes there is no need to call [self setupDesign] or [self setupNavigationBar] as the method [super viewDidLoad] invoked in the viewDidLoad method of the child class will invoke the calls to [self setupDesign]/[self setupNavigationBar] via the super class.

#### Onboarding
The application is helping the user to onboard with the usage of the app in different place:
* Notification onboard alert is shown at the first launch of the app. If user tapped on `Not now`, the notification onboard alert is shown a second time whenever the app is used for the third time or when the user did purchase her first order or when she did create a new account.
* Digest onboard alert is shown only once after the notification onboard alert on the digest view.
* Ogham onboard alert is shown only once when user arrived to the Ogham view.
* Inspiration onboard alert is shown only once when user arrived to the Inspiration view.

#### Status Bar Styles For View Controllers
View controllers within the app are responsible for setting the status bar style (light or dark) as appropriate. View controllers showing dark backgrounds should display a light status bar style (UIStatusBarStyleLight) and those with light backgrounds a dark status bar style (UIStatusBarStyleDefault). The setting of the status bar is done via the implementation of the -(UIStatusBarStyle)preferredStatusBarStyle method in the view controller implementation file where the view controller here returns the status bar style it wishes to use.

Note: UIViewControllers which get pushed onto navigation controllers do NOT by default dictate their own status bar style. So if you specify the -(UIStatusBarStyle)preferredStatusBarStyle method in the viewController it will not take effect as the UINavigationController class the view is pushed in has control of the status bar. In order to allow the UINavigationController to delegate how to dictate the status bar to its pushed view controllers you must:
  1. Implement the -(UIViewController*)childViewControllerForStatusBarStyle method in the UINavigationController implementation file (.m) and return self.viewControllers.lastObject. This will allow the last pushed object (view controller on screen) to dictate is own status bar styling.
  2. Implement the along with the -(UIStatusBarStyle)preferredStatusBarStyle method in the UINavigationController to set the default status bar styling for all pushed view controllers of the UINavigationController.
  3. Implement the (-UIStatusBarStyle)preferredStatusBarStyle method in the view controller implementation file for view controllers which you want to override the status bar styling done by the UINavigationController. If you wish to keep the status bar styling the same as whats provided already by the UINavigationController there is no need to specify the -(UIStatusBarStyle)preferredStatusBarStyle method in the view controller implementation file.

#### Associated Domains and AutoFill
If a user has opted into using Safari Keychain, they can log into the Alex and Ani app using Safari AutoFill.  
Please visit [Matt's blog post]("http://blog.prolificinteractive.com/2015/01/14/sync-ios-app-website-passwords-autofill/") for the explanation, or this [tutorial on Ray Wenderlich]("http://www.raywenderlich.com/90978/secure-passwords-with-safari-autofill-ios-8").

#### Checkout
See [documentation](Checkout.md) for further details.

#### Custom Tab Bar
A custom tab bar is used in order to make sure that the background color/image displayed in the `EmpowermentViewController` persists from tab to tab. So, both the empowerment view and the tab bar are always present on the screen.
The logic for the tab bar lives in `MainViewController`, the two mains functions called to display a ViewController (or tab) are

* `- (void)makeViewControllerActive:(UIViewController *)viewController` to actually display the ViewController
* `-  (void)makeTabActiveAtIndex:(NSUInteger)index;` to update the tab bar

NB: We can easily switch between icon and label in the tab bar with the `UITabStyle` enum (asked couple times in the developement).

#### Hiding Navigation Bar
There are multiple places that we hide the top navigation bar in the initial view controller then show it as the user pushes into new view controllers, e.g. profile and inspiration.  [self.navigationController setNavigationBarHidden:NO animated:YES]; is called in the pushed VCs viewWillAppear to avoid swipe-to-pop scenario where the initial VCs viewWillAppear is called and navigation bar is hidden, then user cancels swipe-to-pop/swipes back to the pushed VC with it's navigation bar hidden.

[self.navigationController setNavigationBarHidden:NO animated:YES];

##### Hidden features
Caution: `EmpowermentViewController` and CYO is now hidden from the UI but the code still exists. For more information, look at [that pull request]("https://bitbucket.org/prolificinteractive/alex-and-ani-ios/pull-request/708/change-the-design-the-the-tab-bar/activity") or [that story]("https://www.pivotaltracker.com/story/show/98692858").

##### Deleted features
Symbol Wall and Shop your Mood are features that were deleted after it was decided that they would not be included in the app now or in the future.  They were deleted in the commit tagged 'DeletingUnusedFeatures'.

#### BaseCategoryDisplayViewController
In several places in the app, we use a custom CollectionView with a custom offset effect between the products' cell, similar to  [Pinterest](https://github.com/chiahsien/CHTCollectionViewWaterfallLayout). The cell for products with only one finish (or skuId) are smaller than those with multiple. When there is multiple finishes, we can switch between them, using a swipe gesture. There are dot indicators at the bottom of the cell, which show the currently selected finish. So, the cell's height increases if there are multiple finishes.
Since the custom CollectionView is used everywhere, we created a base class `AABaseCategoryDisplayViewController`, to easily recreate this view, and make it reusable. It's used in:

* Shop - CDP (Category Display Page)
* CYO (Create Your Own)
    * Add and Edit products
    * Add to Bag
* Profile - Wishlist
    * Want It
    * Have It

  Do the following to use it:

  * Create a ViewController which inherits from `AABaseCategoryDisplayViewController`
  * Create a Storyboard scene for your viewController in the Storyboard (or .xib)
  * Add a CollectionView to that View (in the Storyboard), and create an outlet to `self.collectionView` in the ViewController.
  * Implement methods that specify that they must be overridden. I invite you to follow `AABaseCategoryDisplayViewController.h` header's documentation for that step. (Runtime exceptions are thrown if required methods are not implemented).

  ### Appledoc
  * This project uses appledoc to extract documentation from Objective-C code.
  * [GitHub Repo](https://github.com/tomaz/appledoc)

  ### Install/Build notes
  * If Fabric isn't installed on your computer yet:
    * Go to [fabric.io](https://fabric.io/),
    * Install Xcode plugin,
    * Select 'app already setup' when asked
  * Compile project, you are all set!

  ### Continuous Integration
  To bump build and version numbers, the Makefile uses [AGVTool](https://developer.apple.com/library/ios/qa/qa1827/_index.html)

  * AGVTool is bundled with Xcode
  * To use it, Execute: `agvtool next-version -all`

  This project also uses [Shenzhen](https://github.com/nomad/shenzhen) for creating command line builds.
