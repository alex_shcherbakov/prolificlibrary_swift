#### CreateYourOwn
CreateYourOwn is currently not used in the app.  It may be added again at some point, but until then the code will remain in the project, but will not be displayed in the app.
The CreateYourOwn feature or CYO is kind of complex, so we tried to simplify the code. One thing was to avoid a huge `AACreateYourOwnViewController`.
We achieved that by moving all of scene's logic into the `AACreateYourOwnSceneCoordinator` and the data management into the `AACreateYourOwnDataManager`.

##### CreateYourOwnCoordinator - SceneKit
The `AACreateYourOwnSceneCoordinator` is a Singleton that is used only in `AACreateYourOwnViewController`. The two classes are pretty tightly coupled, but mostly as a result of decentralizing the logic from the view controller.
* Before the view for `AACreateYourOwnViewController` is visible, the Coordinator determines if the view should be filled with a splash screen instructing a user to add a bracelet, or the SceneKit scene, which renders the 3D representations of the bracelets.
* The sceneCoordinator is lazily instantiated in `AACreateYourOwnViewController`, and that's also where the scene gets created, with a passed in frame.
    * The method `- (void)setupSceneWithBraceletStartPosition:(int)startPosition;`, adds the lighting to the scene, adds the floor, and also adds a cone to the scene.
        * The cone was initially created in order to make sure that the selected bracelets do not topple over, but this may not be needed moving forward.
* The Coordinator is a delegate of `AACreateYourOwnDataManagerStackDelegate`, which informs the Coordinator when a bracelet has been added or removed.
    * When a bracelet is added, an asynchronous call is made to download the assets for the bracelet and charms. Though, charms have not been implemented as yet. Additions are received by `- (void)didFinishDownloadingBracelet:(AABracelet *)bracelet`, which calls into `- (void)addBraceletToSceneWithBracelet:(AABracelet *)bracelet` that applies all the maps and places the bracelets on the screen.
        * Before attempting to download a bracelet, the `AACreateYourOwnDataManager` calls into the `AADataStore` and checks to see if the data already exists locally. If it does, a `HTTP HEAD` request is made to make sure the file is the latest version. The data is saved in the cache directory, and will be removed automatically by the OS, in the event that storage space is needed. No other mechanisms for removing loaded data have been created.
    * When a bracelet is removed, `- (void)didRemoveBraceletWithId:(NSString *)productId` is invoked, which removes all the bracelets from the screen and re-adds the remaining bracelets.
* The sceneCoordinator has logic for using a `CMMotionManager`, but it was never really flushed out.

##### CreateYourOwnDataManager
The `AACreateYourOwnDataManager` is a Singleton, and it's called from basically every ViewController of the CYO feature:

* `AACreateYourOwnViewController`
* `AACreateYourOwnSceneCoordinator`
* `AACreateYourOwnAddEditViewController`
* `AACreateYourOwnAddToBagViewController`

None of them have to take care of data management, or deal with the DataStore. They just have to talk to the DataManager, which is responsible for data management and talking to the DataStore.
Please take a look a the `AACreateYourOwnDataManager.h`'s header for the whole documentation.
