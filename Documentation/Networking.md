#### Data loading
This project uses the Facade design pattern, most heavily in `AADataStore`. This class is responsible for providing data to all view controllers by interacting with the network manager, `AAHTTPSessionManager`. `AAHTTPSessionManager`, handles all of the API calls using AFNetworking.

However, when you want to load data from API in controller, you should use `AADataLoader` class. It encapsulates logic involved in network call such as checking local data store for cached copy, showing the activity indicator, handling error if any, etc.

#### Alex & Ani API
![alex and ani API](network_calls_scheme.png)
When the app needs data, we are using a `AADataStore` which is setup with a sessionId. `SessionID`, `Cart` and `User` are stored in the local storage of the user’s phone. For each request to a specific endpoint (with or without parameters) sent done by the `AADataStore`, we are adding in the header the sessionID.
Then, in the API server, for the giving request, another request will be made to the website with the cookie associated to the given sessionID. The API expect an HTML response which will be transformed as a specified JSON response to the App.

With the given JSON response, the app will handle a success response by doing business logic and UI logic. For the case where it’s an error, the app use an `AAErrorHandler` which will also display a banner or an empty state view.

#### Glenlivet
The app is using the [Glenlivet](https://github.com/prolificinteractive/glenlivet) framework to get its data. Glenlivet is scrapping Alex and Ani's website and transforming the HTML data into JSON structures matching the specs. Glenlivet has 3 different environments:

  * Production
  * Staging
  * Develop

#### Session ID
We use 3 methods to handle the expiration of backend Glenlivet cookies:

First, every API error should be handled by `AAErrorHandler`.  `AAErrorHandler` checks if the error is an internet connection or session related error.  If it is a session related error, it signs out the current user, which will create a new session when the user logs back in.

* A hacky one:

```
+ (NSUInteger)sessionAnonymousValid:( void (^)() )anonymousValid
            sessionAnonymousExpired:( void (^)(BOOL) )anonymousExpired
          sessionAuthenticatedValid:( void (^)() )authenticatedValid
        sessionAuthenticatedExpired:( void (^)(BOOL) )authenticatedExpired
                           allCases:( void (^)() )allCases
                       requestError:( void (^)(NSError *error) )requestError;
```

The goal of this method is to simplify the madness of the "session expiration".  
To be precise the website's cookie that Glenlivet isn't keeping alive.  
This method takes six blocks and only one, apart from allCases, will be executed on any given case.  
E.g. authenticatedValid and allCases, anonymousValid and allCases, authenticatedExpired and allCases, etc.

* and a more convenient one:

```
+ (NSUInteger)sessionAuthenticatedValid:( void (^)() )authenticatedValid
            sessionAuthenticatedExpired:( void (^)(BOOL) )authenticatedExpired
                           requestError:( void (^)(NSError *error) )requestError;
```


 Finally, we handle Signs Out User with 3 situations;

 1. `!error` there's no error

 2. `error.code == kSessionErrorLoggedOut` the user was logged out by the backend service

 3. `error.code == kSessionErrorExpired` the backend session identifier expired
