#### Pods used
* [AdobeMobileSDK](https://github.com/Adobe-Marketing-Cloud/mobile-services/releases) : analytics tracker from Adobe.
* [AFNetworking](https://github.com/AFNetworking/AFNetworking) : make all the network API calls. Also used to detect any network status change.
* [AFViewShaker](https://github.com/ArtFeel/AFViewShaker/releases) : simple utility for UIView shake animation to notify user about wrong data entry.
* [Appboy-iOS-SDK](https://github.com/Appboy/appboy-ios-sdk/releases) : marketing tool used for push notifications.
* [CardIO](https://github.com/card-io/card.io-iOS-SDK) : card.io provides fast, easy credit card scanning in mobile apps.
* [CCHMapClusterController](https://github.com/choefele/CCHMapClusterController) : High-performance map clustering with MapKit for iOS and OS X.
* [CHTCollectionViewWaterfallLayout](https://github.com/chiahsien/CHTCollectionViewWaterfallLayout) : CHTCollectionViewWaterfallLayout is a subclass of UICollectionViewLayout, and it tries to imitate UICollectionViewFlowLayout's usage as much as possible. This layout is inspired by Pinterest. It also is compatible with PSTCollectionView.
* [Crashlytics](https://docs.fabric.io/ios/crashlytics/index.html) : crash tracking service integrated with Fabric
* [Fabric](https://docs.fabric.io/ios/index.html) : crash tracking service
* [FBSDKCoreKit](https://github.com/facebook/facebook-ios-sdk/releases) : open-source library allows to integrate Facebook into your iOS app.
* [FXBlurView](https://github.com/nicklockwood/FXBlurView.git) : FXBlurView is a UIView subclass that replicates the iOS 7 realtime background blur effect.
* [GoogleConversionTracking](https://github.com/cheah/GoogleConversionTracking-iOS-SDK/releases) : Google Conversion Tracking iOS SDK for use with CocoaPods
* [Hockey](https://github.com/bitstadium/HockeySDK-iOS) : distribution tool for the app to Prolific and David's Bridal teams, including a crash report tool.
* [HexColors](https://github.com/mRs-/HexColors/releases) : drop in category for HexColor Support for NSColor and UIColor. Support for HexColors with prefixed # and without.
* [JVFloatLabeledTextField](https://github.com/jverdi/JVFloatLabeledTextField) : floating labels framework used in all the UITextField in the app (specially in My Account views).
* [Instabug](https://github.com/Instabug/Instabug-iOS/releases) : bug reporting for iOS apps.
* [PIDatePicker](https://github.com/prolificinteractive/PIDatePicker) : A custom UIDatePicker object that allows design customization of various user interface attributes such as font, color, etc.
* [PaymentKit](https://github.com/prolificinteractive/PaymentKit.git) : PaymentKit is a utility library for writing payment forms in iOS apps.
* [Prolific e-Commerce Framework](https://bitbucket.org/prolificinteractive/prolific-ecommerce-framework) : PCF
* [SDWebImage](https://github.com/rs/SDWebImage) : asynchronous image downloader with cache support to download all the images coming from the API.
* [SMPageControl](https://github.com/Spaceman-Labs/SMPageControl) : UIPageControl’s Fancy One-Upping Cousin.
* [TPKeyboardAvoiding](https://github.com/michaeltyson/TPKeyboardAvoiding) : A drop-in universal solution for moving text fields out of the way of the keyboard in iOS.
* [TSMessages](https://github.com/prolificinteractive/TSMessages.git) : This library provides an easy way to use class to show little notification views on the top of the screen.
* [TTTAttributedLabel](https://github.com/TTTAttributedLabel/TTTAttributedLabel) A drop-in replacement for UILabel that supports attributes, data detectors, links, and more.
* [pop](https://github.com/facebook/pop) : An extensible iOS and OS X animation library, useful for physics-based interactions.
* [UserVoice](https://github.com/uservoice/uservoice-ios-sdk) : native UserVoice framework to provide a feedback tool to the users, and a instant answers tool to customers' questions.
* [Mantle](https://github.com/Mantle/Mantle) : conveniant framework to help writing models and transform JSON data to model objects.
* [Harpy](https://github.com/ArtSabintsev/Harpy/releases) : checks a user's currently installed version of your iOS app against the version that is currently available in the App Store. If a new version is available, an alert can be presented to the user informing them of the newer version, and giving them the option to update the application.
