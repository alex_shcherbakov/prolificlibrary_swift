### Project Documentation

### Cocoapods
* This project relies on [cocoapods](cocapods.org) to manage its dependencies.
* Some of the pods can be found in Prolific's private pods repo. Make sure you have it setup on your computer before setting up the project. [instructions](https://bitbucket.org/prolificinteractive/ios-pispecs)

### Build instructions
* Clone this repo
* Instal cocoapod-keys (see section below)
* Run `pod install`
* Open `Alex and Ani.xcworkspace`

### Cocoapods-Keys
* This project uses cocoapods-keys to store the third party keys and tokens outside of the project code. https://github.com/orta/cocoapods-keys
* To setup cocoapods-keys, run 'gem install cocoapods-keys'
* Move the Keys.plist file into the project.  This is stored on dropbox folder Engineering/iOS/Security/Dev-Keys/alexandani/dev
* Run `make set_keys`, this sets all the keys for the project.

#### Tags
This project is using multiple tags: TODO, TEMP. Every code tags should be linked to a pivotal tracker story. Which can bring more informations and add tasks.
* TODO is a code tag to define that something is not yet implemented but should be done.
* TEMP is a code tag to define that something is temporary and could be removed in future.

#### Branches
This project has three branches: master, staging, and develop.
* Master is the build that is currently on the store.
* Staging is for daily Hockey builds, Alpha builds and TestFlight builds, that are used for QA.
* Develop is the branch that developers are working on and create pull requests into.

#### Build Configurations
There are 4 different build configurations for this app. AppStore is used for App Store builds and TestFlight builds. Develop is used for Hockey builds. Debug is used for developer builds, e.g. simulator. Alpha - for alpha builds.

There are 3 differences between configurations: icon, default environment and ability to switch environments in iPhone settings.

#### CI on Bitrise
Project has 4 Bitrise projects:
* Daily build `Alex and Ani - Hockey` - for internal QA
* Alpha build `Alex & Ani - Hockey Alpha` - for partners QA
* TestFlight build `Alex and Ani - iTunes` - for final testing round and submission to AppStore
* Unit Tests `Alex and Ani - Unit Tests` - runs every time we push/update/merge PRs

#### Objective-C vs Swift
Project uses Swift for all new features and major refactoring.
